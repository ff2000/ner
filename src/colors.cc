/* ner: src/colors.cc
 *
 * Copyright (c) 2012 Michael Forney
 *
 * This file is a part of ner.
 *
 * ner is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License version 3, as published by the Free
 * Software Foundation.
 *
 * ner is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * ner.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <ncurses.h>

#include "colors.hh"

ColorMap defaultColorMap = {
    /* General */
    { Color::CutOffIndicator,           ColorPair{ COLOR_GREEN } },
    { Color::MoreLessIndicator,         ColorPair{ COLOR_BLACK,  COLOR_GREEN } },
    { Color::EmptySpaceIndicator,       ColorPair{ COLOR_CYAN } },
    { Color::LineWrapIndicator,         ColorPair{ COLOR_GREEN } },

    /* Status Bar */
    { Color::StatusBarStatus,           ColorPair{ COLOR_WHITE,  COLOR_BLUE } },
    { Color::StatusBarStatusDivider,    ColorPair{ COLOR_WHITE,  COLOR_BLUE } },
    { Color::StatusBarMessage,          ColorPair{ COLOR_BLACK,  COLOR_WHITE } },
    { Color::StatusBarPrompt,           ColorPair{ COLOR_WHITE } },

    /* Search View */
    { Color::SearchViewDate,                    ColorPair{ COLOR_YELLOW} },
    { Color::SearchViewMessageCountComplete,    ColorPair{ COLOR_GREEN } },
    { Color::SearchViewMessageCountPartial,     ColorPair{ COLOR_MAGENTA } },
    { Color::SearchViewAuthors,                 ColorPair{ COLOR_CYAN } },
    { Color::SearchViewSubject,                 ColorPair{ COLOR_WHITE } },
    { Color::SearchViewTags,                    ColorPair{ COLOR_RED } },

    /* ThreadView */
    { Color::ThreadViewArrow,   ColorPair{ COLOR_GREEN } },
    { Color::ThreadViewDate,    ColorPair{ COLOR_CYAN } },
    { Color::ThreadViewTags,    ColorPair{ COLOR_RED } },

    /* Email View */
    { Color::EmailViewHeader,   ColorPair{ COLOR_CYAN } },

    /* View View */
    { Color::ViewViewNumber,    ColorPair{ COLOR_CYAN } },
    { Color::ViewViewName,      ColorPair{ COLOR_GREEN } },
    { Color::ViewViewStatus,    ColorPair{ COLOR_WHITE } },

    /* Search List View */
    { Color::SearchListViewName,    ColorPair{ COLOR_CYAN } },
    { Color::SearchListViewTerms,   ColorPair{ COLOR_YELLOW } },
    { Color::SearchListViewResults, ColorPair{ COLOR_GREEN } },

    /* Message Parts */
    { Color::AttachmentFilename,    ColorPair{ COLOR_YELLOW } },
    { Color::AttachmentMimeType,    ColorPair{ COLOR_MAGENTA } },
    { Color::AttachmentFilesize,    ColorPair{ COLOR_GREEN } },

    /* Citation levels */
    { Color::CitationLevel1,    ColorPair{ COLOR_GREEN } },
    { Color::CitationLevel2,    ColorPair{ COLOR_YELLOW } },
    { Color::CitationLevel3,    ColorPair{ COLOR_CYAN } },
    { Color::CitationLevel4,    ColorPair{ COLOR_MAGENTA } }
};

// vim: fdm=syntax fo=croql et sw=4 sts=4 ts=8

