/* ner: src/identity_manager.cc
 *
 * Copyright (c) 2010 Michael Forney
 *
 * This file is a part of ner.
 *
 * ner is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License version 3, as published by the Free
 * Software Foundation.
 *
 * ner is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * ner.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "identity_manager.hh"

#include "maildir.hh"
#include "notmuch/config.hh"
#include "ner_config.hh"

#include <yaml-cpp/node/convert.h>

void operator>>(const YAML::Node & node, Identity & identity)
{
    identity.name = node["name"].as<std::string>();
    identity.email = node["email"].as<std::string>();

    /* Optional entries */
    const YAML::Node& signatureNode = node["signature"];
    const YAML::Node& sendNode = node["send"];
    const YAML::Node& sentMailNode = node["sent_mail"];
    const YAML::Node& syncMaildirNode = node["sync_maildir_flags"];

    if (signatureNode)
        identity.signaturePath = signatureNode.as<std::string>();
    else
        identity.signaturePath.clear();

    if (sendNode)
        identity.sendCommand = sendNode.as<std::string>();

    std::string tagPrefix = "tag:the-ner.org,2010:";

    identity.sentMail.reset();

    if (sentMailNode)
    {
        if (sentMailNode.Tag() == tagPrefix + "maildir")
        {
            std::string sentMailPath = sentMailNode.as<std::string>();
            identity.sentMail = std::make_shared<Maildir>(sentMailPath);
        }
    }

    if (syncMaildirNode)
        identity.sync_maildir_flags = syncMaildirNode.as<bool>();
    else
        identity.sync_maildir_flags = NerConfig::instance().sync_maildir_flags;
}

namespace YAML {
    template <>
    struct convert<Identity> {
        static bool decode(Node const& node, Identity& id) {
            node >> id;
            return true;
        }
    };
}

IdentityManager & IdentityManager::instance()
{
    static IdentityManager * manager = NULL;

    if (!manager)
        manager = new IdentityManager();

    return *manager;
}

IdentityManager::IdentityManager()
{
}

IdentityManager::~IdentityManager()
{
}

void IdentityManager::load(const YAML::Node& node)
{
    _identities.clear();

    if (node) {
        _identities = node.as<std::map<std::string, Identity>>();
    }
    /* Otherwise, guess identities from notmuch config */
    else
    {
        Identity identity;
        identity.name = Notmuch::Config::instance().user.name;
        identity.email = Notmuch::Config::instance().user.primary_email;
        _identities.insert(std::make_pair(identity.email, identity));

        for (auto & address : Notmuch::Config::instance().user.other_email)
        {
            identity.email = address;
            _identities.insert(std::make_pair(identity.email, identity));
        }
    }
}

void IdentityManager::setDefaultIdentity(const std::string & identity)
{
    _defaultIdentity = identity;
}

const Identity * IdentityManager::defaultIdentity() const
{
    auto identity = _identities.find(_defaultIdentity);

    if (identity == _identities.end())
        /* We couldn't find it, just use the first one */
        identity = _identities.begin();

    return &identity->second;
}

const Identity * IdentityManager::findIdentityByAddress(const std::string& address)
{
//     std::string email = internet_address_mailbox_get_addr(INTERNET_ADDRESS_MAILBOX(address));

    for (auto identity = _identities.begin(); identity != _identities.end(); ++identity)
    {
        if (identity->second.email == address)
            return &identity->second;
    }

    return 0;
}

const Identity * IdentityManager::findIdentityByKey(const std::string & key)
{
    auto identity = _identities.find(key);

    if (identity != _identities.end())
        return &identity->second;

    return 0;
}

// vim: fo=croql et sw=4 sts=4 ts=8

